#pragma once

#include <string>
#include <vector>
#include <set>

/*
 * ParseFacts stores all facts from _line string into the _facts set.
 * _line in format "FACT1","FACT2",..."FACTn".
 * _facts is a set of {FACT1, FACT2, ..., FACTn}
 */
void ParseFacts(const std::string &_line, std::set<std::string> &_facts);

/*
 * ParseRules splits string with sequence of rules, separated by comma, to array of different rules.
 * _line is a string in format "FORMULA1->FACT1",...,"FORMULAn->FACTn".
 * _rules is a vector of {FORMULA1->FACT1, ..., FORMULAn->FACTn}.
 */
void ParseRules(const std::string &_line, std::vector<std::string> &_rules);

/*
 * ParseRule extracts name of new fact and its formula from string representation of the rule.
 * _line is a string in format FACT1,FACT2,...,FACTn|...|FACTm,...,FACTk->NEW_FACT
 * _facts is a vector of sets {FACT1, FACT2, ..., FACTn}, ..., {FACTm, ..., FACTk}
 * _newFact is a name of new fact (NEW_FACT).
 */
void ParseRule(const std::string &_line, std::vector<std::set<std::string> > &_facts, std::string &_newFact);