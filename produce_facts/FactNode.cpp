#include "FactNode.h"

using namespace std;

FactNode::FactNode(const context_ptr &_context, const bool _value)
	: m_formula(), m_context(_context), m_value(_value), m_defined(_value), m_visited(false), m_axiom(_value) {}

bool FactNode::Value() {
	if (m_defined) {
		return m_value;
	}
	if (m_visited) {
		return false;
	}

	m_visited = true;
	m_value = Or();
	m_defined = true;
	m_visited = false;

	return m_value;
}

void FactNode::AddConjunction(const conjunction_t &_conjunction) {
	m_formula.insert(_conjunction);
}

bool FactNode::Or() const {
	bool result = false;
	for (auto it = begin(m_formula); !result && it != end(m_formula); ++it) {
		result = result || And(*it);
	}
	return result;
}

bool FactNode::And(const conjunction_t &_conjunction) const {
	bool result = true;
	for (auto it = begin(_conjunction); result && it != end(_conjunction); ++it) {
		auto node = m_context->find(*it);
		if (node == m_context->end()) {
			result = false;
		}
		else {
			result = node->second->Value();
		}
	}
	return result;
}

bool FactNode::IsAxiom() const {
	return m_axiom;
}