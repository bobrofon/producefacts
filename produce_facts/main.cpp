#include <cstdlib>
#include <memory>
#include <iostream>
#include <fstream>
#include <boost\algorithm\string\join.hpp>
#include "Utils.h"
#include "FactNode.h"

using namespace std;
using boost::join;

/*
 * ProduceFacts fill _newFacts set by new facts that follow from applying _rules to _facts.
 * _facts - predefined axioms in format "AXIOM1","AXIOM2",..."AXIOMn".
 * _rules - rules in format "A1,A2,...An|...|Am,...,Ak->NEW_FACT",...
 * _newFact - result set.
 */
void ProduceFacts(const string &_facts, const string &_rules, set<string> &_newFacts) {
	FactNode::context_ptr context = make_shared<FactNode::context_t>();
	set<string> axioms;

	ParseFacts(_facts, axioms);
	for (const auto &x : axioms) {
		(*context)[x] = make_shared<FactNode>(context, true);
	}

	vector<string> rules;
	ParseRules(_rules, rules);
	for (const auto &r : rules) {
		string name;
		vector<set<string> > formula;
		ParseRule(r, formula, name);

		shared_ptr<FactNode> node;
		if (context->find(name) == context->end()) {
			node = make_shared<FactNode>(context);
			(*context)[name] = node;
		}
		else {
			node = (*context)[name];
		}

		for (const auto &conjantion : formula) {
			node->AddConjunction(conjantion);
		}
	}
	
	for (auto &node : (*context)) {
		if (node.second->Value() && !node.second->IsAxiom()) {
			_newFacts.insert(node.first);
		}
	}
}

void Usage(const string &_progName) {
	cerr << "ProduceFacts is a command-line utility for producing new facts from existing axioms." << endl << endl;
	cerr << "Use: " << _progName << " input_file" << endl;
	cerr << " imput_file - file with axioms and rules in specific format." << endl;
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		Usage(argv[0]);
		return EXIT_FAILURE;
	}
	
	ifstream inputStream(argv[1]);
	if (!inputStream) {
		cerr << "Can't open file " << argv[1] << endl;
		return EXIT_FAILURE;
	}

	string axioms;
	string rules;
	getline(inputStream, axioms);
	getline(inputStream, rules);

	set<string> newFacts;
	ProduceFacts(axioms, rules, newFacts);

	cout << join(newFacts, ", ") << endl;

	return EXIT_SUCCESS;
}