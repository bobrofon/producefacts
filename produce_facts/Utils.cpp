#include "Utils.h"
#include <boost\tokenizer.hpp>
#include <boost\algorithm\string\trim.hpp>

using namespace std;
using boost::tokenizer;
using boost::char_separator;
using boost::trim;

void ParseFacts(const string &_line, set<string> &_facts) {
	char_separator<char> sep("\", ");
	tokenizer<char_separator<char> > tokens(_line, sep);
	copy(begin(tokens), end(tokens), inserter(_facts, begin(_facts)));
}

void ParseRules(const string &_line, vector<string> &_rules) {
	char_separator<char> sep("\"");
	tokenizer<char_separator<char> > tokens(_line, sep);
	for (const string &s : tokens) {
		if (s.find("->") != string::npos) {
			_rules.push_back(s);
		}
	}
}


void ParseConjunction(const string &_line, set<string> &_conjunction) {
	char_separator<char> sep(", ");
	tokenizer<char_separator<char> > tokens(_line, sep);
	copy(begin(tokens), end(tokens), inserter(_conjunction, begin(_conjunction)));
}

void ParseRule(const string &_line, vector<set<string> > &_facts, string &_newFact) {
	auto arrowPos = _line.find("->");
	assert(arrowPos != string::npos);
	_newFact = _line.substr(arrowPos + 2);
	trim(_newFact);

	char_separator<char> sep = char_separator<char>("|");
	string rule = _line.substr(0, arrowPos);
	trim(rule);
	tokenizer<char_separator<char> > tokens(rule, sep);

	for (const string &s : tokens) {
		set<string> conjunction;
		ParseConjunction(s, conjunction);
		_facts.push_back(conjunction);
	}
}