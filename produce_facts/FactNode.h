#pragma once

#include <set>
#include <string>
#include <map>
#include <memory>

class FactNode
{
public:
	typedef std::map<std::string, std::shared_ptr<FactNode> > context_t;
	typedef std::shared_ptr<context_t> context_ptr;
	typedef std::set<std::string> conjunction_t;
	typedef std::set<conjunction_t> formula_t;

	FactNode(const context_ptr &_context, const bool _value = false);

	/*
	 * Value returns true if this formula is an axiom or can be gotten from other axiom.
	 * In other cases Value returns false.
	 */
	bool Value();

	/*
	 * AddConjunction adds new conjunction to current formula.
	 * Formula is deducible if at least one of conjunction is deducible.
	 */
	void AddConjunction(const conjunction_t &_conjunction);

	/*
	 * IsAxiom returns true if this formula is a predefined axiom.
	 * In other cases IsAxiom returns false.
	 */
	bool IsAxiom() const;

private:
	bool Or() const;
	bool And(const conjunction_t &_conjunction) const;

	formula_t m_formula;
	context_ptr m_context;
	bool m_value;
	bool m_defined;
	bool m_visited;
	bool m_axiom;
};